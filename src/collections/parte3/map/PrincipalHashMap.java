package collections.parte3.map;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class PrincipalHashMap {

    public static void main(String[] args) {

        OperacionesProductoHashMap opp = new OperacionesProductoHashMap();
        try {
            opp.Menu();
        } catch (IOException ex) {
            Logger.getLogger(PrincipalHashMap.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
