package collections.parte2.queue;

import java.util.Objects;

public class Empleado implements Comparable<Empleado> {
    private String nombre;
    private double salario;

    public Empleado(String nombre, double salario) {
        super();
        this.nombre = nombre;
        this.salario = salario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    @Override
    public String toString() {
        return "Empleado [nombre=" + nombre + ", salario=" + salario + "]";
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, salario);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Empleado employee = (Empleado) o;
        return Double.compare(employee.salario, salario) == 0 &&
                Objects.equals(nombre, employee.nombre);
    }

    @Override
    public int compareTo(Empleado o) {
        if (this.getSalario() > o.getSalario()) {
            return 1;
        } else if (this.getSalario() < o.getSalario()) {
            return -1;
        } else {
            return 0;
        }
    }

}
