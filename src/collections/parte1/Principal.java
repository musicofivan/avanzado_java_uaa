package collections.parte1;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Principal {
    public static void main(String[] args) {
        OperacionesProducto opp = new OperacionesProducto();
        try {
            opp.Menu();
        } catch (IOException ex) {
            Logger.getLogger(Principal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
