package collections.parte1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EjemploForEach {

    public static void main(String[] args) {

        List<String> list = Arrays.asList("Lars", "Simon");


        List<String> anotherList = new ArrayList<>();
        anotherList.add("Lars");
        anotherList.add("Simon");


        list.forEach(System.out::println);
        anotherList.forEach(System.out::println);


        List<Integer> listInteger = Arrays.asList(3, 2, 1, 4, 5, 6, 6);


        for (Integer integer : listInteger) {
            System.out.println(integer);
        }

    }

}
