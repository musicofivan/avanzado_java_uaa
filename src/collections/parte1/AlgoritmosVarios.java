package collections.parte1;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class AlgoritmosVarios {

    private Character[] letras = {'P', 'C', 'M'};
    private Character[] copiaLetras;
    private List<Character> lista;
    private List<Character> copiaLista;

    public AlgoritmosVarios() {
        lista = Arrays.asList(letras);
        copiaLetras = new Character[3];

        copiaLista = Arrays.asList(copiaLetras);
        System.out.println("Lista inicial: ");
        imprimir(lista);

        Collections.reverse(lista);
        System.out.println("\nDespues de llamar a reverse: ");
        imprimir(lista);

        Collections.copy(copiaLista, lista);
        System.out.println("\nDespues de copy: ");
        imprimir(copiaLista);

        Collections.fill(lista, 'R');
        System.out.println("\nDespues de llamar a fill: ");
        imprimir(lista);
    }

    private void imprimir(List<Character> refLista) {
        System.out.print("La lista es: ");

        for (Character elem : refLista) {
            System.out.printf("%s ", elem);
        }
        System.out.printf("\nMax: %s", Collections.max(refLista));
        System.out.printf(" Min: %s\n", Collections.min(refLista));
    }

    public static void main(String[] args) {
        new AlgoritmosVarios();
    }
}
